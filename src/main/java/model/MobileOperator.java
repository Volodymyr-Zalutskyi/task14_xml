package model;

public class MobileOperator {
    private String tariff;
    private String operator;
    private double payroll;
    private String internet;
    private String calls;
    private String sms;

    public String getTariff() {
        return tariff;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public double getPayroll() {
        return payroll;
    }

    public void setPayroll(double payroll) {
        this.payroll = payroll;
    }

    public String getInternet() {
        return internet;
    }

    public void setInternet(String internet) {
        this.internet = internet;
    }

    public String getCalls() {
        return calls;
    }

    public void setCalls(String calls) {
        this.calls = calls;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    @Override
    public String toString() {
        return "\nMobileOperator{" +
                "tariff - " + tariff + '\'' +
                "operator='" + operator + '\'' +
                ", payroll=" + payroll +
                ", internet='" + internet + '\'' +
                ", calls='" + calls + '\'' +
                ", sms='" + sms + '\'' +
                '}';
    }
}
