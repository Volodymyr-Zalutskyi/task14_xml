import model.MobileOperator;
import parser.DOMParsedXML;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.List;

public class Parser {
    public static void main(String[] args) {
        List<MobileOperator> operatorLis = DOMParsedXML.parserMobileOperatorXML();
        System.out.println(operatorLis);

        try {
            File inputFile = new File("C:/Users/Volodymyr/IdeaProjects/_14_xml/src/main/resources/tariff.xml");
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            UserHandler userhandler = new UserHandler();
            saxParser.parse(inputFile, userhandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
