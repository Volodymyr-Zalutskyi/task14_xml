import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class DomParser {
    public static void main(String[] args) {
        try {
            File inputFile = new File("C:/Users/Volodymyr/IdeaProjects/_14_xml/src/main/resources/tariff.xml");

            //Get Document Builder
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            //Build Document
            Document doc = dBuilder.parse(inputFile);

            //Normalize the XML Structure; It's just too important !!
            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            //Get all employees
            NodeList nList = doc.getElementsByTagName("tariff");

            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    System.out.println("Tariff plan : " + eElement.getAttribute("tariff"));
                    System.out.println("Operator : " + eElement.getElementsByTagName("operator").item(0).getTextContent());
                    System.out.println("Payroll : " + eElement.getElementsByTagName("payroll").item(0).getTextContent());
                    System.out.println("Internet : " + eElement.getElementsByTagName("internet").item(0).getTextContent());
                    System.out.println("Calls : " + eElement.getElementsByTagName("calls").item(0).getTextContent());
                    System.out.println("SMS : " + eElement.getElementsByTagName("sms").item(0).getTextContent());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
