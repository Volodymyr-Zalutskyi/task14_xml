import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class UserHandler extends DefaultHandler {

    boolean operator = false;
    boolean payroll = false;
    boolean internet = false;
    boolean calls = false;
    boolean sms = false;

    @Override
    public void startElement(
            String uri, String localName, String qName, Attributes attributes)
            throws SAXException {

        if (qName.equalsIgnoreCase("tariff")) {
            String name = attributes.getValue("name");
            System.out.println("Tariff : " + name);
        } else if (qName.equalsIgnoreCase("operator")) {
            operator = true;
        } else if (qName.equalsIgnoreCase("payroll")) {
            payroll = true;
        } else if (qName.equalsIgnoreCase("internet")) {
            internet = true;
        } else if (qName.equalsIgnoreCase("calls")) {
            calls = true;
        } else if (qName.equalsIgnoreCase("sms")) {
            sms = true;
        }
    }

    @Override
    public void endElement(String uri,
                           String localName, String qName) throws SAXException {

        if (qName.equalsIgnoreCase("tariff")) {
            System.out.println("End Element :" + qName);
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {

        if (operator) {
            System.out.println("Operator: " + new String(ch, start, length));
            operator = false;
        } else if (payroll) {
            System.out.println("Payroll: " + new String(ch, start, length));
            payroll = false;
        } else if (internet) {
            System.out.println("Internet: " + new String(ch, start, length));
            internet = false;
        } else if (calls) {
            System.out.println("Calls: " + new String(ch, start, length));
            calls = false;
        } else if (sms) {
            System.out.println("SMS: " + new String(ch, start, length));
            sms = false;
        }
    }

}
