package parser;

import model.MobileOperator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DOMParsedXML {
    public static List<MobileOperator> parserMobileOperatorXML() {
        //Initialize a list of mobile operator
        List<MobileOperator> operators = new ArrayList<>();
        MobileOperator mobileOperator = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new File("C:/Users/Volodymyr/IdeaProjects/_14_xml/src/main/resources/tariff.xml"));
            document.getDocumentElement().normalize();
            NodeList nList = document.getElementsByTagName("tariff");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node node = nList.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    //Create new Employee Object
                    mobileOperator = new MobileOperator();
                    mobileOperator.setTariff(eElement.getAttribute("tariff"));
                    mobileOperator.setOperator(eElement.getElementsByTagName("operator").item(0).getTextContent());
                    mobileOperator.setPayroll(Double.parseDouble(eElement.getElementsByTagName("payroll").item(0).getTextContent()));
                    mobileOperator.setInternet(eElement.getElementsByTagName("internet").item(0).getTextContent());
                    mobileOperator.setCalls(eElement.getElementsByTagName("calls").item(0).getTextContent());
                    mobileOperator.setSms( eElement.getElementsByTagName("sms").item(0).getTextContent());
                    //Add Employee to list
                    operators.add(mobileOperator);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return operators;
    }
}
