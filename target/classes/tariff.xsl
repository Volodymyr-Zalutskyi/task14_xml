<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h2>Mobile Tariffs</h2>
                <table border="2">
                    <tr bgcolor="#9acd32">
                        <th>Operator</th>
                        <th>Payroll</th>
                        <th>Internet</th>
                        <th>Calls</th>
                        <th>Sms</th>
                    </tr>
                    <xsl:for-each select="mobileoperator/tariff">
                        <tr>
                            <td><xsl:value-of select="operator" /></td>
                            <td><xsl:value-of select="payroll" /></td>
                            <td><xsl:value-of select="internet" /></td>
                            <td><xsl:value-of select="calls" /></td>
                            <td><xsl:value-of select="sms" /></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>